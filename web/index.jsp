<%-- 
    Document   : index
    Created on : 19-nov-2019, 9:45:54
    Author     : Joan Sanmartí Escudero
--%>

<%@page import="java.util.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Parque Natural Sierra Bicuerca</title>
        <link rel="stylesheet" href="./css/estilo.css">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

    </head>

    <body>
        <header>
            <img class="img-full" src="imagenes/parque.jpg" alt="Foto del parque">
            <h1>Parque Natural <span class="titcol"> Sierra Bicuerca</span></h1>
        </header>


        <section>
            <aside>
                <p>Área del rectangulo = ${rectangulo.calcularArea()}</p>
                <jsp:include page="/jsp/date.jsp" flush="true"/>
                <nav>
                    <ul>
                        <li class="mb-5 active"><a href="index.jsp">Inicio</a></li>
                        <li class="mb-5"><a href="especies.jsp">Especies</a></li>
                        <li class="mb-5"><a href="#">Reservas</a></li>
                        <li><a href="#">Galeria</a></li>
                    </ul>
                </nav>
            </aside>

            <div>

                <h2 class="ml-5">Bienvenidos al Parque Natural</h2>

                <p class="pl-5"> El Parque Natural de Sierra Bicuerca, reserva de la biosfera, es referencia 
                    obligada del bosque y matorral mediterr&aacuteneo interior, y h&aacutebitat de la fauna 
                    y flora aut&oacutectona. 
                </p>

                <p class="pl-5"> Fue declarado parque natural en 1983, con una superficie 
                    de 9.090 Has. siendo uno de los enclaves m&aacutes extensos de la Comunidad Valenciana.
                    Con un gran espesor de vegetaci&oacuten, y varios riachuelos ofrecen un clima ideal 
                    para las especies terrestres y acu&aacuteticas. Sus bosques, de carrasca y pino, 
                    albergan una amplia colonia de aves.
                </p>

                <p class="pl-5"> Entre las especies del parque podemos destacar una de las &uacuteltimas colonias de buitre 
                    leonado de la zona. Tambi&eacuten nos veremos constantemente sobrevolados por el halc&oacuten 
                    peregrino o el &aacuteguila perdicera. Al acercarnos a los riachuelos, veremos numerosas 
                    pisadas de jabal&iacutes y ciervos, y si no hacemos mucho ruido, no es dif&iacutecil encontrar 
                    alguno abrevando bajo la atenta mirada del mart&iacuten pescador, esperando a la despistada trucha.
                </p>

                <table border="3">
                    <%
                        for (int i = 0; i <= 10; i++) {
                    %>
                    <tr>
                        <td>N&uacutemero</td>
                        <td><%= i%></td>
                    </tr>  
                    <%
                        }
                    %>
                </table>

            </div>



        </section>
        <footer>

        </footer>
    </body>



</html>

<%-- 
    Document   : especies
    Created on : 19-nov-2019, 11:07:54
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Parque Natural Sierra Bicuerca</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./css/estilo.css">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
    </head>
    <body>
        <header>
            <img class="img-full" src="imagenes/parque.jpg" alt="">
            <h1>Parque Natural <span class="titcol"> Sierra Bicuerca</span></h1>
        </header>

        <section>
            <aside>
                <nav>
                    <ul>
                        <li class="mb-5"><a href="index.jsp">Inicio</a></li>
                        <li class="mb-5  active"><a href="especies.jsp">Especies</a></li>
                        <li class="mb-5"><a href="#">Reservas</a></li>
                        <li><a href="#">Galeria</a></li>
                    </ul>
                </nav>
            </aside>
            <div>
                <h2 class="ml-5">Especies</h2>
                <p class="pl-5">Algunas especies que viven en el parque son:</p>
                <h3 class="pl-5">Nutria</h3>
                <p class="pl-5">La nutria europea o pale&aacutertica (Lutra lutra) es un mam&iacutefero carn&iacutevoro de h&aacutebitat acu&aacutetico. Se alimenta de peces, ranas y pequeñas presas que caza en el r&iacuteo. Cerca de sus lugares de cobijo, podremos ver toboganes que crean para llegar r&aacutepidamente al agua.</p>
                <h3 class="pl-5">Buitre leonado</h3>
                <p class="pl-5">El buitre leonado (Gyps fulvus) es un ave rapaz, una de las mayores que puede encontrarse en la Pen&iacutensula Ib&eacuterica. Su alimentaci&oacuten principal es la carroña, que localiza con su aguda vista.</p>
                <h3 class="pl-5">Gineta</h3>
                <p class="pl-5">La gineta (Genetta genetta) es una especie de mam&iacutefero carn&iacutevoro. Son grandes trepadoras, muy &aacutegiles y grandes cazadoreas de pequeños animales. Aunque tambien se alimentan de los frutos del bosque que se peden encontrar en el parque.</p>

            </div>
        </section>
    </body>
</html>

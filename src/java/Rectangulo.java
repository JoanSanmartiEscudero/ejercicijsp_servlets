/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Usuario
 */
public class Rectangulo {
    
    float altura,base,area,perimetro;
    Coordenades coordenades;

    public Rectangulo(float altura, float base) {
        this.altura = altura;
        this.base = base;
    }

    public Coordenades getCoordenades() {
        return coordenades;
    }
    
    public float calcularArea(){
        this.area = this.altura*this.base;
        return this.area;
    }
    public float calcularPerimetro(){
        this.perimetro= (altura*2)+(base*2);
        return this.perimetro;
    }
    
    
}
